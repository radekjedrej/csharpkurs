﻿using System;

namespace StudentDiary
{
    class Program
    {
        static void Main(string[] args)
        {
            Diary diary = new Diary();

            diary.AddRating(5);
            diary.AddRating(8.5f);
            diary.AddRating(4.7f);

            DiaryStatistics stats = diary.ComputeStatistics();

            float avg = stats.AverageGrade;
            float minValue = stats.MinGrade;
            float maxValue = stats.MaxGrade;

            Console.WriteLine("Srednia wartosc ocen to: " + avg);
            Console.WriteLine("Minimalna wartosc ocen to: " + minValue);
            Console.WriteLine("Maksymalna wartosc ocen to: " + maxValue);


            Diary diary2 = new Diary();

            diary2.AddRating(4);
            diary2.AddRating(9.5f);
            diary2.AddRating(1.7f);

            stats = diary2.ComputeStatistics();

            float avg2 = stats.AverageGrade;
            float minValue2 = stats.MinGrade;
            float maxValue2 = stats.MaxGrade;

            Console.WriteLine("Srednia wartosc ocen to: " + avg2);
            Console.WriteLine("Minimalna wartosc ocen to: " + minValue2);
            Console.WriteLine("Maksymalna wartosc ocen to: " + maxValue2);

            Console.ReadKey();

            //float avg = diary.CalculateAverage();
            //float max = diary.GiveMaxRating();
            //float min = diary.GiveMinRating();

            //for (; ;)
            //{
            //    Console.WriteLine("Podaj Ocene z zakresu 1- 10: ");
            //    float rating;
            //    bool result = float.TryParse(Console.ReadLine(), out rating);

            //    if (rating == 11)
            //    {
            //        break;
            //    }

            //    if (result)
            //    {
            //        if (rating < 11 && rating > 0)
            //        {
            //            diary.AddRating(rating);
            //        }
            //        else
            //        {
            //            Console.WriteLine("Niepoprawna liczba, podaj liczbe z zakresu 1- 10");
            //        }
            //    }

            //}

            //Console.WriteLine("Srednia Twoich Ocen To: " + diary.CalculateAverage());
            //Console.WriteLine("Najwyzsza ocena to: " + diary.GiveMaxRating());
            //Console.WriteLine("Najnizsza ocena to: " + diary.GiveMinRating());
            //Console.ReadKey();
        }
    }
}
