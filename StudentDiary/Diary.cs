﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StudentDiary
{
    class Diary
    {
        public Diary()
        {
            ratings = new List<float>();
        }

        // Stan (zmienne - pola)
        List<float> ratings;

        internal DiaryStatistics ComputeStatistics()
        {
            DiaryStatistics stats = new DiaryStatistics();

            float total = 0;

            foreach (var rating in ratings)
            {
                total += rating;
            }

            stats.AverageGrade = total / ratings.Count;
            stats.MaxGrade = ratings.Max();
            stats.MinGrade = ratings.Min();

            return stats;
        }

        // Zachowania
        public void AddRating(float rating)
        {
             ratings.Add(rating);
        }
    }
}
