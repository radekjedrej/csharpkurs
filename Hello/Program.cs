﻿using System;

namespace Hello
{
    class Program
    {
        static void Main(string[] args)
        {
            ValueTypes();

            for (; ;)
            {
                Greeting();
                CheckAge();
                Settings();
            }
        }

        private static void ValueTypes()
        {
            int maxInt = int.MaxValue;
            int minInt = int.MinValue;
            long maxLong = long.MaxValue;
            long minLong = long.MinValue;

            Console.WriteLine("Max Int " + maxInt);
            Console.WriteLine("Min Int " + minInt);
            Console.WriteLine("Max Long " + maxLong);
            Console.WriteLine("Min Long " + minLong);
        }

        private static void Settings()
        {
            Console.ReadKey();
            Console.Clear();
            Console.ResetColor();
        }

        /// <summary>
        /// Based on age write correct message to the user
        /// </summary>
        private static void CheckAge()
        {
            Console.Write("Wpisz ile masz lat: ");
            int age;
            bool result = int.TryParse(Console.ReadLine(), out age);

            if (age > 17)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("Super brachu moze browarka?");
            }
            else if (result == false)
            {
                Console.WriteLine("Wprowadziles niepoprawny wiek");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("To moze chociaz szklanke soku?");
            }
        }

        /// <summary>
        /// Write welcome message to the user
        /// </summary>
        private static void Greeting()
        {
            Console.Write("Wpisz swoje Imie: ");
            string name = Console.ReadLine();
            Console.WriteLine("Witaj " + name);
        }
    }
}
