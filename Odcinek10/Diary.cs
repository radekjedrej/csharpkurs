﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Odcinek10
{
    class Diary
    {
        public Diary()
        {
            ratings = new List<float>();
        }

        // Stan (zmienne - pola)
        List<float> ratings;

        // Zachowania
        public void AddRating(float rating)
        {
                ratings.Add(rating);
        }

        /// <summary>
        /// Calculate Average Number From All Grades
        /// </summary>
        /// <returns></returns>
        public float CalculateAverage()
        {
            float total = 0;
            float avg = 0;

            foreach (var rating in ratings)
            {
                total += rating;
            }

            avg = total / ratings.Count;

            return avg;
        }

        public float GiveMaxRating()
        {
            return ratings.Max();
        }

        public float GiveMinRating()
        {
            return ratings.Min();
        }
    }
}
