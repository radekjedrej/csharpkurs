﻿using System;

namespace Odcinek10
{
    class Program
    {
        static void Main(string[] args)
        {
            Diary diary = new Diary();
            diary.AddRating(3);
            diary.AddRating(5);

            Diary diary2 = diary;
            diary.AddRating(8);

            Diary diary3 = diary2;
            diary.AddRating(9);
        }
    }
}
