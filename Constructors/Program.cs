﻿using System;

namespace Constructors
{
    class Program
    {
        static void Main(string[] args)
        {
            Person person = new Person();
            Person person2 = new Person(1, "Janusz");
            Person person3 = new Person(1, "Janusz", "Zdubigniew");
            Person person4 = new Person(1, "Janusz", "Zdubigniew", "Krolewiec", 19, "Male");
        }
    }
}
